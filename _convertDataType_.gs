/***********************************************************************************************************************
 * Function to convert the data type from one platform to it's equvierlent on another platform. 
 * Warning.  The current functionality allows for bi-directional conversion but the mapping does not currently support 
 * this behaviour, i.e. the matrix has one to many relationships.  This design was choose to provide a stable and 
 * flexible function name and signature which could be enhanced internally in future rather releasing a function that 
 * will have a limited purpose and cause later code inconsistencies.
 *
 * @fromDataType {object} input the data type value(s) to be converted.  This can either be a string or array of 
 *                        strings.
 * @fromPlatform {string} input the platform where the source data type is from.  Supported platforms are "MySQL" and 
 *                        "BigQuery", case insensative.
 * @toPlatform   {string} input the platform where the destination data type is going to.  Supported platforms are 
 *                        "MySQL" and "BigQuery", case insensative.
 *
 * @return {object} output the data type value(s) after their convertion.  This can either be a string or array of 
 *                  string depending on what was provided in the "fromDataType" parameter.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-04 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _convertDataType_(fromDataType, fromPlatform, toPlatform) {
  
    var dataTypeMap = [
      {MYSQL:"binary",    BIGQUERY:"BYTES"},
      {MYSQL:"varbinary", BIGQUERY:"BYTES"},      
      {MYSQL:"bit",       BIGQUERY:"INTEGER"},
      {MYSQL:"smallint",  BIGQUERY:"INTEGER"},
      {MYSQL:"tinyint",   BIGQUERY:"INTEGER"},         
      {MYSQL:"int",       BIGQUERY:"INTEGER"},
      {MYSQL:"bigint",    BIGQUERY:"INTEGER"},
      {MYSQL:"varchar",   BIGQUERY:"STRING"},
      {MYSQL:"char",      BIGQUERY:"STRING"},
      {MYSQL:"text",      BIGQUERY:"STRING"},
      {MYSQL:"datetime",  BIGQUERY:"DATETIME"},
      {MYSQL:"date",      BIGQUERY:"DATETIME"},
      {MYSQL:"time",      BIGQUERY:"TIME"},
      {MYSQL:"timestamp", BIGQUERY:"TIMESTAMP"},
      {MYSQL:"float",     BIGQUERY:"FLOAT"},
      {MYSQL:"decimal",   BIGQUERY:"FLOAT"},
      {MYSQL:"double",    BIGQUERY:"FLOAT"},
      {MYSQL:"real",      BIGQUERY:"FLOAT"},
      {MYSQL:"bool",      BIGQUERY:"BOOLEAN"}  
    ];

  //Validate Parameters ------------------------------------------------------------------------------------------------
  //Returns null if either platform type is outside of the conversion scope.
  if(dataTypeMap[0][fromPlatform.toUpperCase()] == undefined) {
    _log_('ERROR', 'Source platform "' + fromPlatform + '" is not supported');  
    return null; 
  }
  if(dataTypeMap[0][toPlatform.toUpperCase()] == undefined) {
    _log_('ERROR', 'Destination platform "' + toPlatform + '" is not supported');  
    return null; 
  }

  //One or Many Lookups ------------------------------------------------------------------------------------------------
  if(Array.isArray(fromDataType)) {
    //Multiple lookups
    var toDataType = [];
    for (var i=0; i < fromDataType.length; i++) {
      toDataType.push(lookupDataType(fromDataType[i], fromPlatform, toPlatform));      
      
    }
  } else if (typeof(fromDataType) == 'string') {
    //Single lookup
    var toDataType;  
    toDataType = lookupDataType(fromDataType, fromPlatform, toPlatform);
  } else {
    //Invalid lookup
    _log_('ERROR', 'Expected conversion input must be either an array or a string.');  
    return null; 
  }
  
  return toDataType;
  
  //Lookup Function ----------------------------------------------------------------------------------------------------
  function lookupDataType(fromDataType, fromPlatform, toPlatform) {
    
    var toDataType;
    
    //Lookup the BigQuery data type equiverlent of it's MySQL datatype
    for (var i=0; i < dataTypeMap.length; i++) {
      if (dataTypeMap[i][fromPlatform.toUpperCase()] === fromDataType.toLowerCase()) {
        toDataType = dataTypeMap[i][toPlatform.toUpperCase()];
        break;
      } 
    }
    //Error message to support investigation when downstream failure occurs.    
    if(toDataType == null) {
      _log_('ERROR', 'Lookup failed, attempting to convert "' + fromDataType + 
            '" from "' + fromPlatform + '" to "' + toPlatform + '". ');  
    }
    
    return toDataType;
  }
}