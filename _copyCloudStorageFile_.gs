/***********************************************************************************************************************
 * This function confirms the existence or absence of a specified file in Cloud Storage.
 *
 * @csBucketSource   {string} input the source bucket name.  Google Cloud Storage uses a flat namespace, so all bucket 
 *                            names are unique even without the inclusion of the project name.  The name should _not_ 
 *                            include a "gs://" prefix.
 * @csFileNameSource {string} input the source file name, prefixed with the folder path if one exists.  
 *                            E.g. "your_folder/your_file.txt"
 * @csBucketTarget   {string} input the target bucket name.  Google Cloud Storage uses a flat namespace, so all bucket 
 *                            names are unique even without the inclusion of the project name.  The name should _not_ 
 *                            include a "gs://" prefix.
 * @csFileNameTarget {string} input the target file name, prefixed with the folder path if one exists.  
 *                            E.g. "your_folder/your_file.txt".  If the file already exist it will be overwritten.
 * 
 * @return {boolean} returns true if the copy was successful and false if an error was encountered.
 *
 * Version History
 * ---------------
 * 1.0 2017-10-02 Created
 **********************************************************************************************************************/ 
function _copyCloudStorageFile_(csBucketSource, csFileNameSource, csBucketTarget, csFileNameTarget) {
  
  var token = _getAuthenticationToken_();
  
  //TODO
  //Confirm source bucket and file both exits, if not log error and return false
  //Confirm target bucket exists, if not log error and return false
  //Check if target file exists, if it does log a warning that it'll be overwritten
  
  try {
    // Create the header authorisation  
    var headers = {
      "Authorization": "Bearer " + token
    };
    // Create the Cloud SQL instances get parameters
    var parameters = {
      "method": "post",    
      "headers": headers,
      "muteHttpExceptions": true
    }; 

    // Create the url of the storage API    
    var api = 
      "https://www.googleapis.com/storage/v1/b/" + 
      csBucketSource + "/o/" + encodeURIComponent(csFileNameSource) + 
      "/rewriteTo/b/" + 
      csBucketTarget + "/o/" + encodeURIComponent(csFileNameTarget);
    
    var response = UrlFetchApp.fetch(api, parameters); 
    
    if (response.getResponseCode() == 200) {
      _log_('INFO', 'Copied file ' + 
        'from "gs://' + csBucketSource + '/' + csFileNameSource + '" ' + 
        'to "gs://' + csBucketTarget + '/' + csFileNameTarget + '" successfully.');
      return true;
    } else if (response.getResponseCode() == 404) {
      _log_('ERROR', 'Copy failed with 404 not found.');
      return false;
    }
    _log_('ERROR', 'unexpected response (' + response.getResponseCode() + ') ' + response.getContentText());
    return false;
    
  } catch(err) {
    _log_('ERROR', 'Copying file ' + 
          'from "gs://' + csBucketSource + '/' + csFileNameSource + '" ' + 
          'to "gs://' + csBucketTarget + '/' + csFileNameTarget + '" ' + err);
    return false;
  }    	
}