/***********************************************************************************************************************
 * This function sets a the values in a table export request in the cloudSQLExportAudit table.  The status lifecycle 
 * should always match one of the following patterns:
 *  - WAITING - > EXPORTING -> COMPLETE (Normal operation)
 *  - WAITING - > EXPORTING -> TIMEOUT  (Combined queue wait and export time too long)
 *  - WAITING - > EXPORTING -> ERROR    (Output failed)
 *  - WAITING - > TIMEOUT               (Queue wait too long, review simultaneous exports and file sizes)
 *  - WAITING - > ERROR                 (Issue encountered while in queue) 
 *  - WAITING - > EMPTY                 (Expected zero rows of data, empty file created instead of using export) 
 *
 * @projectId    {string} input the Project ID where the data planned for export resides.
 * @sqlInstance  {string} input the Cloud SQL Instance name where the data planned for export resides.
 * @exportID     {string} input the unique ID for the export.
 * @exportStatus {string} input the status of the export which must be updated in the admin table.  Valid options are;
 *                        'WAITING', 'EXPORTING', 'TIMEOUT', 'ERROR', 'EMPTY' or 'COMPLETE'.
 *
 * @return {boolean} returns true if successful and false if an error was encountered.
 *
 * Version History
 * ---------------
 * 1.0 2017-07-10 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 * 1.2 2017-09-20 Process altered to include new 'exportRunTime' and 'exportStartDtTm' columns as well as a new 
 *                'EXPORTING' status.
 * 1.3 2017-09-28 WaitingTime was incorrectly being calculated if it crossed minute boundary.  Corrected.
 * 1.4 2017-10-03 Added new status "EMPTY" for exports of zero rows.
 **********************************************************************************************************************/ 
function _setCloudSQLExportAudit_(projectId, sqlInstance, exportID, exportStatus) {
      
  var dbStmt = _getCloudSQLStatement_(projectId, sqlInstance, G_SQL_EDW_AUDIT_SCHEMA);

  var dbSQL = "";  
  //Common update statement begining, regardless of the status change scenario.
  dbSQL = dbSQL + "UPDATE ";
  dbSQL = dbSQL + " `cloudSQLExportAudit` ";
  dbSQL = dbSQL + "SET "; 
  dbSQL = dbSQL + " `exportStatus`    = '" + exportStatus + "', "; 
  
  //WAITING.  
  //This is never set in this function, it's the default value when the row is inserted into the queue.
  
  //EXPORTING
  if(exportStatus == 'EXPORTING') {
    //Set the status to EXPORTING (done in common statement above)
    //Set the wait time (now - queue entry time)
    //Set the exportStartDtTm (now)
    dbSQL = dbSQL + " `exportWaitTime`  = TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `createdDtTm`)), ";
    dbSQL = dbSQL + " `exportStartDtTm` = CURRENT_TIMESTAMP "; 
  }
  //COMPLETE
  if(exportStatus == 'COMPLETE') {
    //Set the status to COMPLETE (done in common statement above)
    //Set the export run time (now - export start time)
    //Set the completed time (now)
    dbSQL = dbSQL + " `exportRunTime` = TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `exportStartDtTm`)), ";
    dbSQL = dbSQL + " `completedDtTm` = CURRENT_TIMESTAMP "; 
  }
  //TIMEOUT or ERROR
  if(exportStatus == 'TIMEOUT' || exportStatus == 'ERROR') {
    //This logic works for both TIMEOUT and ERROR.
    //This logic works for both TIMEOUT entry routes, either WAITING -> TIMEOUT or EXPORTING -> TIMEOUT
    //Set the status to TIMEOUT
    //Set the wait time (now - queue entry time) only if it's currently NULL (if status was WAITING).
    //Set the export run time (now - export start time) if it's NULL still NULL (if status was WAITING).
    //Set the completed time (now)
    dbSQL = dbSQL + " `exportWaitTime` = IFNULL(exportWaitTime, TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `createdDtTm`))), ";
    dbSQL = dbSQL + " `exportRunTime`  = TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `exportStartDtTm`)), "; 
    dbSQL = dbSQL + " `completedDtTm`  = CURRENT_TIMESTAMP "; 
  }
  //EMPTY
  if(exportStatus == 'EMPTY') {
    //Set the status to EMPTY (done in common statement above)
    //Set the wait time to zero because empty exports don't queue.
    //Set the export run time (now - queue entry time) to represent the empty file copying time.
    //Set the export start time (queue entry time)
    //Set the complete time (now)
    dbSQL = dbSQL + " `exportWaitTime`  = 0, ";
    dbSQL = dbSQL + " `exportRunTime`   = TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `createdDtTm`)), ";
    dbSQL = dbSQL + " `exportStartDtTm` = `createdDtTm`, "; 
    dbSQL = dbSQL + " `completedDtTm`   = CURRENT_TIMESTAMP "; 
  }
  
  //Common where clause to all scenario's 
  //Once an export is complete it can't be timed out by any peer processes.
  dbSQL = dbSQL + "WHERE "; 
  dbSQL = dbSQL + " `exportID` = " + exportID + " ";
  dbSQL = dbSQL + " AND ";
  dbSQL = dbSQL + " `exportStatus` != 'COMPLETE' ";
  
  try {
    dbStmt.executeUpdate(dbSQL);
    dbStmt.close();
    _log_('DEBUG', 'Export queue updated.  Export ID (' + exportID + ') change to status (' + exportStatus + ')');
  } catch(err) {
    _log_('ERROR', 'Setting Cloud SQL Export "' + exportID + '" in Audit failed: ' + err);
    return false;
  }
  
  return true;
}
