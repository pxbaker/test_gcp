/***********************************************************************************************************************
 * Function to establish a database connection and provide an object to access the database.
 * 
 * @projectId {string} input the Project ID
 * @sqlInstance {string} input the Cloud SQL Instance name
 * @sqlSchema {string} input the Cloud SQL database/schema 
 *
 * @return {object} returns a database statement into which SQL can be added and execution.
 *
 * Version History
 * ---------------
 * 1.0 2017-07-10 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/ 
function _getCloudSQLStatement_(projectId, sqlInstance, sqlSchema) {

  var ipAddress = _getInstanceIpAddress_(projectId, sqlInstance);    
  
  // Create the database connection string
  var dbUrl = 'jdbc:mysql://' + ipAddress + ':' + G_MYSQL_PORT + '/' + sqlSchema;
  
  try {
    _log_('DEBUG', 'Connecting to: ' + dbUrl + ',' + G_SERVICE_ACCOUNT + ',********'); 
    
    var dbConn = Jdbc.getConnection(dbUrl, G_SERVICE_ACCOUNT, G_SERVICE_PASSWORD);
    
    //This code has been retained for future reference.
    /*
    var dbConn = Jdbc.getConnection(dbUrl + '?useSSL=true', {
                                    user: SERVICE_ACCOUNT,
                                    password: SERVICE_PASSWORD,
                                    _serverSslCertificate: SERVER_SSL_CERTIFICATE,
                                    _clientSslCertificate: CLIENT_SSL_CERTIFICATE,
                                    _clientSslKey: SERVER_SSL_KEY
                                    });            
    */
          
    var dbStmt = dbConn.createStatement();
    
    return dbStmt;
    
  } catch(err) {
    _log_('ERROR', 'Get Cloud SQL statement failed: ' + err);
  } 
}  