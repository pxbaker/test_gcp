/***********************************************************************************************************************
 * Function to delete a file in Cloud Storage.
 *
 * @csBucket   {string} input the bucket name.  Google Cloud Storage uses a flat namespace, so all bucket names are 
 *                      unique even without the inclusion of the project name. 
 * @csFileName {string} input the file name to be deleted, prefixed with the folder path if one exists.  E.g. 
 *                      "your_folder/your_file.txt"
 *
 * @return {boolean} output the function returns true if the file was deleted successfully and false if an error was 
 *                   encountered. 
 *
 * Version History
 * ---------------
 * 1.0 2017-09-05 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _deleteCloudStorageFile_(csBucket, csFileName) {
  
  var token = _getAuthenticationToken_();
  
  // Create the header authorisation  
  var headers = {
    "Authorization": "Bearer " + token
  };
  // Create the Cloud SQL instances get parameters
  var parameters = {
    "method": "delete",    
    "headers": headers,
    "muteHttpExceptions": true
  };
    
  // Create the url of the storage API
  var api = "https://www.googleapis.com/storage/v1/b/" + csBucket + "/o/" + encodeURIComponent(csFileName);
  // Use the url fetch service to issue the https request and capture the response
  
  try {
    var response = UrlFetchApp.fetch(api, parameters); 
    //Deletion is asynchronous, this loop keeps the function synchronous.
    while (_cloudStorageFileExists_(csBucket, csFileName)) {
      Utilities.sleep(100);
    }
  } catch(err) {
    _log_('ERROR', 'Deleting file "gs://' + csBucket + '/' + csFileName + '" failed: ' + err);
    return false;
  }
  
  return true;
}