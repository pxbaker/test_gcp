/***********************************************************************************************************************
 * Function acts as a custom wrapper to the Google API Stackdriver logging functions, allowing the quantity and detail 
 * of logging to be configured project wide from one location.   
 *
 * @messageType {string} input the type of message to be logged.  The valid options permitted will be mapped as follows:
 *                        - INFO       (console.info)    Hight level information to confirm normal operation
 *                        - DEBUG      (console.log)     Low level information to support development or investigation
 *                        - PARAMETERS (console.log)     Json enclosed list of parameter names and values
 *                        - STATISTICS (console.log)     Data volumetric information
 *                        - WARNING    (console.warn)    Warning information to support risk monitoring
 *                        - ERROR      (console.error)   Failure information to support issue resolution
 *                        - START_TIME (console.time)    Start time information to measure function performance
 *                        - END_TIME   (console.timeEnd) End time information to measure function performance
 * @messageBody {object} input a string or json object containing the information to be logged.
 *
 * @return {boolean} output the function returns true if the information was logged successfully and false if an error 
 *                   was encountered. 
 *
 * Version History
 * ---------------
 * 1.0 2017-09-07 Created.
 **********************************************************************************************************************/
function _log_(messageType, messageBody)
{
  //TODO
  //Add an additional check against a global logging flag used to adjust the detail/volume logged.

  var successful = true;
  
  try {
    switch (messageType.toUpperCase()) {
      case 'INFO':
        console.info(messageBody);
        break;
      case 'DEBUG':
      case 'PARAMETERS':
      case 'STATISTICS':  
        console.log(messageBody);
        break;
      case 'WARNING':
        console.warn(messageBody);
        break;         
      case 'ERROR':
        console.error(messageBody);
        break; 
      case 'START_TIME':
        console.time(messageBody);
        break;
      case 'END_TIME':
        console.timeEnd(messageBody);
        break;
      default: 
        console.log(messageBody);
        console.error('Error.  _log_ does not recognise the message type "' + messageType + '"');
        successful = false;
    } 
    
    //If messageBody is JSON object then stringify it before it's printed in the log.
    //Otherwise the output will just but "object" rather than the data contained.
    if(typeof(messageBody) == 'object') {
      messageBody = JSON.stringify(messageBody);
    }
    
    //Also output to the editor log as an closer reference during development.  
    switch (messageType.toUpperCase()) {
      case 'INFO':
        Logger.log('[' + messageType + '] - ' + messageBody); 
        break;
      case 'DEBUG':
      case 'PARAMETERS':
      case 'STATISTICS':
        Logger.log('[' + messageType + '] - ' + messageBody); 
        break;
      case 'WARNING':
        Logger.log('[' + messageType + '] - ' + messageBody);
        break;         
      case 'ERROR':
        Logger.log('[' + messageType + '] - ' + messageBody); 
        break; 
      case 'START_TIME':
      case 'END_TIME':
        Logger.log('[' + messageType + '] Function ' + messageBody + '() - ' + (new Date())); 
        break;
      default: 
        Logger.log('[' + messageType + '] - ' + messageBody); 
        Logger.log('[ERROR] - _log_ does not recognise the message type "' + messageType + '"'); 
        successful = false;
    }        
  } catch(err) {
    console.error('Error.  _log_ encountered an error: ' + err);
    Logger.log('Error.  _log_ encountered an error: ' + err); 
    successful = false;
  }
  
  return successful;
}