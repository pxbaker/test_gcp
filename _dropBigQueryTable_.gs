/***********************************************************************************************************************
 * Function to drop a BigQuery table.  Drop is used in the function name to be consistent with database terminology but 
 * the Google API uses remove.  
 *
 * @projectId  {string} input the Project ID where the table to drop resides.
 * @bqDataset  {string} input the BigQuery dataset ID where the table to drop resides.
 * @sqlTable   {string} input the BigQuery table where the table to drop resides.
 *
 * @return {boolean} output the function returns true if the table was dropped successfully and false if an error was 
 *                   encountered. 
 *
 * Version History
 * ---------------
 * 1.0 2017-09-04 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _dropBigQueryTable_(projectId, bqDataset, sqlTable) {  
  try { 
    var job = BigQuery.Tables.remove(projectId, bqDataset, sqlTable);
    _log_('DEBUG', 'Dropped BigQuery table "' + sqlTable + '" successful.');
    return true;
  } catch(err) { 
    _log_('ERROR', 'Dropped BigQuery table "' + sqlTable + '"  failed.' + err);	
    return false;   
  } 
}