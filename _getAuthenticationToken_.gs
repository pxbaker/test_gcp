/***********************************************************************************************************************
 * This script replaced:
 *  - getService.gs
 *  - authenticate.gs
 * Previously these scripts populated a global token object.  This design returns the token to be used in each 
 * individual function which requires it.
 *
 * Version History
 * ---------------
 * 1.0 2017-08-01 Created
 * 1.1 2017-08-01 Function renamed with underscores to hide it, i.e. it will not appear in the exposed API.
 * 1.2 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/  
function _getAuthenticationToken_() {
  // Check we have access to the service
  var service = getService();
  if (!service.hasAccess()) {
    var authorizationUrl = service.getAuthorizationUrl();
    _log_('INFO', 'Open the following URL and re-run the script: ' + authorizationUrl);
    return;
  }

  Logger.log('Passed Authentication');
  
  //Get the Access Token
  return service.getAccessToken();

  function getService() {
    // Create a new service with the given name. The name will be used when
    // persisting the authorized token, so ensure it is unique within the
    // scope of the property store.
    return OAuth2.createService('jlr-edw-dev-service')

    // Set the endpoint URLs, which are the same for all Google services.
    .setAuthorizationBaseUrl('https://accounts.google.com/o/oauth2/auth')
    .setTokenUrl('https://accounts.google.com/o/oauth2/token')
  
    // Set the client ID and secret, from the Google Developers Console.
    .setClientId(CLIENT_ID)
    .setClientSecret(CLIENT_SECRET)

    // Set the name of the callback function in the script referenced
    // above that should be invoked to complete the OAuth flow.
    .setCallbackFunction('authCallback')

    // Set the property store where authorized tokens should be persisted.
    .setPropertyStore(PropertiesService.getUserProperties())

    // Set the scopes to request (space-separated for Google services).
    // this is admin access for the sqlservice and access to the cloud-platform:
    .setScope(
      'https://www.googleapis.com/auth/sqlservice.admin ' + 
      'https://www.googleapis.com/auth/cloud-platform')
    
    //Removed because this Should be covered by cloud-platform
    //'https://www.googleapis.com/auth/devstorage.read_write ' 
    
    // Below are Google-specific OAuth2 parameters.

    // Sets the login hint, which will prevent the account chooser screen
    // from being shown to users logged in with multiple accounts.
    .setParam('login_hint', Session.getActiveUser().getEmail())
  
    // Requests offline access.
    .setParam('access_type', 'offline')

    // Forces the approval prompt every time. This is useful for testing,
    // but not desirable in a production application.
    .setParam('approval_prompt', 'force');
  }

  function authCallback(request) {
    var cloudSQLService = getService();
    var isAuthorized = cloudSQLService.handleCallback(request);
    
    if (isAuthorized) {
      _log_('INFO', 'Access Approved');
      return HtmlService.createHtmlOutput('Success! You can close this tab.');
    } else {
      _log_('INFO', 'Access Denied');
      return HtmlService.createHtmlOutput('Denied. You can close this tab');
    }
  }
}


