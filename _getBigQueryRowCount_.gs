/***********************************************************************************************************************
 * Function to get the total number of rows in a BigQuery table.
 *
 * @projectId  {string} input the Project ID where the data to count resides.
 * @bqDataset  {string} input the BigQuery dataset ID where the data to count resides
 * @sqlTable   {string} input the BigQuery table where the data to count resides
 *
 * @return {int} output the number of rows in the table.  If an error is encountered NULL is returned.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-04 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _getBigQueryRowCount_(projectId, bqDataset, sqlTable) {
  if(_bigQueryTableExists_(projectId, bqDataset, sqlTable)) {
    // Get the properties of the target BigQuery table
    try {
      var response = BigQuery.Tables.get(projectId, bqDataset, sqlTable);
      var dataAll = JSON.parse(response);
      var rowCount = dataAll.numRows;
      return parseInt(rowCount);
    } catch(err) {
      _log_('ERROR', 'Could not getting row count for table "' + sqlTable + '": ' + err);
      return null;
    } 
  } else {
    return null;
  }
}