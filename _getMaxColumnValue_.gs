/***********************************************************************************************************************
 * Function to get the maximum value from a column in a BigQuery table.  Useful when performing delta operations to get
 * the highest last created or updated date and time value.
 *
 * @projectId  {string} input the Project ID where the data to search resides.
 * @bqDataset  {string} input the BigQuery dataset ID where the data to search resides
 * @sqlTable   {string} input the BigQuery table where the data to search resides
 * @columnName {string} input the Column where the data to search resides
 *
 * @return {string} output the maximum value found.  If an error is encountered NULL is returned.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-04 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _getMaxColumnValue_(projectId, bqDataset, sqlTable, columnName) {       
  try {
    var request = {
      kind: 'bigquery#queryRequest',
      query: 'SELECT max(' + columnName + ') FROM [' + projectId + ':' + bqDataset + '.' + sqlTable + ']',
      defaultDataset: {
        projectId: projectId,
        datasetId: bqDataset
      }
    };
  
    var response = BigQuery.Jobs.query(request, projectId);
    var dataAll = JSON.parse(response);
    var maxValue = dataAll.rows[0].f[0].v;
  
    _log_('DEBUG', 'Found max value "' + maxValue + '" in column ' + sqlTable + '.' + columnName);          
    return maxValue;
  
  } catch(err) { 
      //Messages changed from Error to Warning since this doesn't break the process it just impacts performance.
      _log_('WARNING', 'Unable to find column ' + sqlTable + '.' + columnName);	
      return null;   
  } 	
}	


