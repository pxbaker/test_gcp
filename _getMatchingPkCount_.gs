/***********************************************************************************************************************
 * Function compares the contents of two tables.  The tables must have identical primary key(s) structures but the 
 * remaining columns are out of scope.  For each primary key value which is found to exist in both tables, i.e. a single 
 * unique join, the count retuned will be incremented by 1.
 *
 * @projectId    {string} input the Project ID where the two tables reside.
 * @bqDataset    {string} input the BigQuery dataset ID where the two tables reside.
 * @sqlTable     {string} input the BigQuery table to compare (the order of these two parameters is irrelevant).
 * @bqDeltaTable {string} input the BigQuery table to compare (the order of these two parameters is irrelevant).
 * @tablePKs     {string} input an array of primary key column names, expect in both tables.
 *
 * @return {int} output the number of rows in the tables with matching PK values.  If an error is encountered NULL is 
 *               returned.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-05 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _getMatchingPkCount_(projectId, bqDataset, sqlTable, bqDeltaTable, tablePKs) {   

  //Build ON statement using primary key columns
  var onStatement = '';
  for(pkIndex = 0; pkIndex < tablePKs.length; pkIndex++) {
    if(pkIndex != 0) onStatement += ' AND ';
    onStatement += 'CurrentTable.' + tablePKs[pkIndex] + ' = DeltaTable.' + tablePKs[pkIndex];    
  }
      
  var bqJoinDefn = '';
  bqJoinDefn = bqJoinDefn + ' SELECT COUNT(*) ';
  bqJoinDefn = bqJoinDefn + ' FROM [' + projectId + ':' + bqDataset + '.' + sqlTable + '] AS CurrentTable '; 
  bqJoinDefn = bqJoinDefn + ' JOIN [' + projectId + ':' + bqDataset + '.' + bqDeltaTable + '] AS DeltaTable '; 
  bqJoinDefn = bqJoinDefn + ' ON ' + onStatement;  

  var request = {
    kind: 'bigquery#queryRequest',
    query: bqJoinDefn,
    defaultDataset: {
      projectId: projectId,
      datasetId: bqDataset
    }
  };	
  
  try {
    var response = BigQuery.Jobs.query(request, projectId);
    var dataAll = JSON.parse(response);
    var rowCount = dataAll.rows[0].f[0].v;
    return parseInt(rowCount);
  } catch(err) { 
    _log_('ERROR', 'Failed to join "' + sqlTable + '" to "' + bqDeltaTable + '", due to error: ' + err);  
    return null;   
  } 	
  
}
