/***********************************************************************************************************************
 * Function to establish a database connection and provide an object to access the database.
 * 
 * @projectId   {string} input the Project ID where the Cloud SQL instance is located.
 * @sqlInstance {string} input the Cloud SQL Instance name.
 *
 * @return {string} returns the I.P. address of the database instance or null if an error was encountered.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-06 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/ 
// Function to get the ip address of a given CloudSQL instance
function _getInstanceIpAddress_(projectId, sqlInstance) {
  
  var token = _getAuthenticationToken_();
    
  // Create the header authorisation  
  var headers = {
    "Authorization": "Bearer " + token
  };
      
  // Create the Cloud SQL instances get parameters
  var parameters = {
    "method": "get",    
    "headers": headers,
    "instance": sqlInstance,
    "project": projectId,
    "muteHttpExceptions": true
  };
      
  // Create the url of the sql instances get API	
  var api = "https://www.googleapis.com/sql/v1beta4/projects/" + projectId + "/instances/" + sqlInstance + "?fields=ipAddresses";	

  try {
    // Use the url fetch service to issue the https request and capture the response
    var response = UrlFetchApp.fetch(api, parameters);    
      
    // Extract the ip address of the instance from the response
    var content = JSON.parse(response.getContentText());
      
    return content.ipAddresses[0].ipAddress; 
  } catch(err) {
    _log_('ERROR', 'Getting ' + sqlInstance + ' instance ip address failed: ' + err);
    return null;
  }
}