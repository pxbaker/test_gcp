/***********************************************************************************************************************
 * This function polls the cloudSQLExportAudit table to see if the given exportID can now be executed and the table 
 * exported using the CloudSQL Admin API.  If it's not ready this function will wait for up to 4 minutes before 
 * returning false as an indication of timming out. 
 *
 * @projectId {string} input the Project ID where the data planned for export resides.
 * @sqlInstance {string} input the Cloud SQL Instance name where the data planned for export resides.
 * @exportID {int} input the ID given to this data export operation, unique to the lifespan of the project.
 *
 * @return {boolean} returns true if the given exportID has arrived at the head of the export queue and false if timmed
 *  out.
 *
 * Version History
 * ---------------
 * 1.0 2017-07-10 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 * 1.2 2017-09-20 Widen the head of the queue to include both the oldest waiting or current exporting table.
 * 1.3 2017-09-28 WaitingTime was incorrectly being calculated if it crossed minute boundary.  Corrected.
 * 1.4 2017-10-03 Added recordset and database connection closure.
 *                Replaced endless loop with conditional check.
 **********************************************************************************************************************/ 
function _readyToExportTable_(projectId, sqlInstance, exportID) {

  var startDtTm = new Date().getTime();

  var oldestExportID; 
  var oldestWaitingTime; 
  
  var dbStmt = _getCloudSQLStatement_(projectId, sqlInstance, G_SQL_EDW_AUDIT_SCHEMA);
  
  //Write SQL statement to get the head of the queue, 
  //i.e. the oldest entry which has been waiting longest.
  var dbSQL = "";
  dbSQL = dbSQL + "SELECT ";
  dbSQL = dbSQL + " `exportID`, ";
  
  //dbSQL = dbSQL + " ((CURRENT_TIMESTAMP() - `createdDtTm`) * 1000) AS WaitingTime "; 
  dbSQL = dbSQL + " TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `createdDtTm`)) * 1000 AS WaitingTime "; 

  dbSQL = dbSQL + "FROM "; 
  dbSQL = dbSQL + " `cloudSQLExportAudit` "; 
  dbSQL = dbSQL + "WHERE "; 
  dbSQL = dbSQL + " `exportStatus` = 'WAITING' OR `exportStatus` = 'EXPORTING' ";
  dbSQL = dbSQL + "ORDER BY "; 
  dbSQL = dbSQL + " `createdDtTm` ASC "; 
  dbSQL = dbSQL + "LIMIT ";
  dbSQL = dbSQL + " 1 ";
  
  var readyToExport = null;
  
  try {     
    //Loop until a response is set;.
    while (readyToExport == null) {
      
      //Refresh the head of the queue
      var rs = dbStmt.executeQuery(dbSQL);
      rs.next();
      oldestExportID = rs.getString(1); 
      oldestWaitingTime = rs.getString(2); 

      //Refresh the current running time for this function.
      var runningFunctionDtTm = new Date().getTime() - startDtTm;
      
      if(oldestExportID == exportID) {
        //If the head of the queue is "our" table,      
        //Return true so the table export can begin
        readyToExport = true;
      
      //TODO make sure these number are correct.
      } else if (runningFunctionDtTm > G_EXPORT_TIMEOUT_MILLISECONDS) {
        //If the function run time exceeds the timeout limit,
        //then close off Self in Admin database as timed out.
        _setCloudSQLExportAudit_(projectId, sqlInstance, exportID, 'TIMEOUT');
        
        //Return false so the table export can throw an timeout exception
        readyToExport = false;
        
      } else if (oldestWaitingTime > G_EXPORT_TIMEOUT_MILLISECONDS) {
        //If the head of the queue is beyond the timeout,
        //then close offdata:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAL0lEQVR42mNgAIIDBw78Z0ADcDEQA4aRJVHEkAWwacCqiAGbnUTrxmk/Xkfi8iYAOLJ03URXP6kAAAAASUVORK5CYII= this Orphan in Admin database as timed out.
        _setCloudSQLExportAudit_(projectId, sqlInstance, oldestExportID, 'TIMEOUT');
        //continue looping
        
      } else {
        //The wait is inside an else to avoid waiting if the head of the queue was a timeout.
        //I.e. the queue is re-checked immediately rather than waiting.
        Utilities.sleep(100);
        
      }
    }  
  } catch(err) {
    _log_('ERROR', 'While waiting to perform export "' + exportID + '" an error was encountered: ' + err);
  } 
  
  rs.close();
  dbStmt.close(); 
  
  return readyToExport;
}