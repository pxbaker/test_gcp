/***********************************************************************************************************************
 * This function puts a new table export request in the cloudSQLExportAudit table in the status WAITING, while it waits 
 * to be processed serially along with other table export requests in this Cloud SQL instance. 
 *
 * @projectId       {string} input the Project ID where the data planned for export resides.
 * @sqlInstance     {string} input the Cloud SQL Instance name where the data planned for export resides.
 * @sqlExportSchema {string} input the Schema/Database name where the data planned for export resides.
 * @sqlExportTable  {string} input the Table name where the data planned for export resides.
 * @rowCount        {int} input number of rows in the source select and expected in the output file. 
 *
 * @return {int} returns the request ID if successfully added and null if an error was encountered.
 *
 * Version History
 * ---------------
 * 1.0 2017-07-10 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 * 1.2 2017-10-03 Added rowCount parameter.  Added recordset and database connection closure.
 **********************************************************************************************************************/ 
function _putCloudSQLExportAudit_(projectId, sqlInstance, sqlExportSchema, sqlExportTable, rowCount) {
  
  //Default to null for a failure.
  var exportID = null;
      
  var dbStmt = _getCloudSQLStatement_(projectId, sqlInstance, G_SQL_EDW_AUDIT_SCHEMA);

  try {
    var dbSQL = "";
    
    //Using absence of column name and value in SQL to default exportStatus to 'WAITING'. 
    //Using absence of column name and value in SQL to default createdDtTm to <now>. 
    dbSQL = dbSQL + "INSERT INTO cloudSQLExportAudit ";
    dbSQL = dbSQL + " (exportSchema, exportTable, exportRowCount) "; 
    dbSQL = dbSQL + "VALUES ";
    dbSQL = dbSQL + " ('" + sqlExportSchema + "', '" + sqlExportTable + "', " + rowCount + ") ";
    var rs = dbStmt.executeUpdate(dbSQL);
    
    //Get the last ID inserted.  
    //These can't be coupled into one execution because they're a read and write, which use different functions.
    //But the risk of a different ID getting in between these statements is minimal.
    dbSQL = "SELECT LAST_INSERT_ID() ";
    var rs = dbStmt.executeQuery(dbSQL);
    rs.next();
    exportID = rs.getString(1);  

    //Close the connections
    rs.close();
    dbStmt.close();
    
    _log_('DEBUG', 'Export ID ('  + exportID + ') added to queue.');
  } catch(err) {
    _log_('ERROR', 'Putting Cloud SQL Export into Audit failed: ' + err);
  } 		

  return exportID
}
