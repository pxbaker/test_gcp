/***********************************************************************************************************************
 * Global Variables 
 * These values are used by functions across the full scope of the Google Apps Script project.
 *
 * Version History
 * ---------------
 * 1.0 
 *  |  2017-03-17 Released prior to the version history changes being documented.
 * 2.9 
 * 3.0 2017-07-24 Added service account username, password and ssl certificates. 
 * 3.1 2017-07-25 Added "G_" prefix to some variables.
 **********************************************************************************************************************/  
var G_BQ_DATASET_LABEL_KEY_ZERO_DOWNTIME = 'zero-downtime';
var G_BQ_DATASET_LABEL_VALUE_ZERO_DOWNTIME_ON = 'on';
var G_BQ_DATASET_LABEL_VALUE_ZERO_DOWNTIME_OFF = 'off';

var G_MYSQL_PORT = '3306';

//4 minutes
var G_EXPORT_TIMEOUT_MILLISECONDS = 240000;

var G_SQL_EDW_AUDIT_SCHEMA = 'EDW_ADMIN';

var G_DELTA_COLUMN_UPDATED = 'last_updated_dttm';
var G_DELTA_COLUMN_CREATED = 'created_dttm';
  
var G_NULL_MARKER = '<NULL_VALUE>';