/***********************************************************************************************************************
 * Function to get the total number of rows in a Cloud SQL set of data.
 *
 * @projectId    {string} input the Project ID
 * @sqlInstance  {string} input the Cloud SQL Instance name
 * @sqlSchema    {string} input the Cloud SQL database/schema 
 * @sqlStatement {string} input the scope of the data to be counted.  This can be any of the following:
 *                          - A table name
 *                          - A view name
 *                          - A query
 *
 * @return {int} output the number of rows in the sql results.  If an error is encountered NULL is returned.
 *
 * Version History
 * ---------------
 * 1.0 2017-10-02 Created
 **********************************************************************************************************************/
function _getCloudSQLRowCount_(projectId, sqlInstance, sqlSchema, sqlStatement) {
  
  var rowCount = null;
    
  var dbStmt = _getCloudSQLStatement_(projectId, sqlInstance, sqlSchema);

  try {
    var dbSQL = "";
    
    //The SQL for a table or view as the source parameter is slightly different to a query.
    if(sqlStatement.match(/[ ]*SELECT/gi) == null) {
      dbSQL = dbSQL + "SELECT COUNT(*) FROM " + sqlStatement + " rowCountData ";
    } else {
      dbSQL = dbSQL + "SELECT COUNT(*) FROM (" + sqlStatement + ") rowCountData ";
    }

    // Execute the SQL Statement and get the first element of the query result set
    var rs = dbStmt.executeQuery(dbSQL);
    rs.next();
    rowCount = rs.getString(1);     
    
    // Close the connections
    rs.close();
    dbStmt.close(); 
  } catch(err) {
    _log_('ERROR', 'Getting Cloud SQL row count failed: ' + err);
    return null;
  }   
  
  return parseInt(rowCount);
}
