function test_deployment()
{   
    Logger.log('Running Test Code.');
    Logger.log('-------------------');
    
	Logger.log('Result 7 + 1 = ' + getIncrement('7'));

    //Request this dataset and label is created for us to do the test.
  	Logger.log('Value of "toggle" label is ' + getLabelValue('jlr-dl-edw-test', 'framework_test', 'toggle'));
  
    return 'toogle=' + getLabelValue('jlr-dl-edw-test', 'framework_test', 'toggle');
    
    Logger.log('-------------------');
    Logger.log('Completed Test Code');
}

function test_ip()
{   
  Logger.log('jlr-dl-edw-ods IP = ' + _getInstanceIpAddress_('jlr-dl-edw-test', 'jlr-dl-edw-ods'));
}


function exportTest()
{   
  var projectId = 'jlr-dl-edw-test';
  var sqlInstance = 'jlr-dl-edw-ods';
  var sqlSchema = 'information_schema';
  var sqlTable = 'tables';  
  var bqDataset = 'framework_test';
  var csBucket = 'jlr-dl-edw-test-framework-test';
  var exportSQL = 'select * from information_schema.tables'
  var csFileName = 'tables_test.csv';
  
  Logger.log('Running Test Code.');
  Logger.log('-------------------');
  
  Logger.log('Result ' +   exportCloudSQLTableCSV(projectId, sqlInstance, sqlSchema, sqlTable, exportSQL, csBucket, csFileName));
  
  Logger.log('-------------------');
  Logger.log('Completed Test Code');
  
  
}