/***********************************************************************************************************************
 * Function to interrogate a specified table for a range of information.  E.g. schema details and row counts.  Due to 
 * the diverse information collected the results must be returned in a JSON object.
 *
 * @projectId   {string} input the Project ID where the table resides.
 * @sqlInstance {string} input the Cloud SQL Instance name where the table resides.
 * @sqlSchema   {string} input the Schema/Database name where the table resides.
 * @sqlTable    {string} input the Table name.
 * @maxDate     {string} input the max date value, if provided a delta selection statement will be created.
 * @nullMarker  {string} input the string value with which to replace any null values in the export SQL statements.
 *
 * @return {json}.tablePK array containing any column names in the tables primary key.
 * @return {json}.pkFieldExists boolean, true ff the table has a primary key and falise if it doesn't.
 * @return {json}.tableDescription description of the table.
 * @return {json}.columnName array containing all column names in table.
 * @return {json}.columnDataType array containing each columns MySQL data type.
 * @return {json}.columnDescription array containing each columns description.
 * @return {json}.updateFieldExists boolean, true if the table has a timestamp column which support delta extracts.
 * @return {json}.exportAllSQL an SQL statement to select all data in the table.
 * @return {json}.exportDeltaSQL an SQL statement to select only new or changed data in the table.
 * @return {json}.rowCountAll the total number of rows in the table.
 * @return {json}.rowCountDelta the number of rows in the table which are new or changed.
 * @return {json}.nullMarker string value with which to replace any null values in the export SQL statements.
 * @return {json}.toString a summary of all values held in the properties above.  Useful for debug purposes.
 *
 * @customfunction
 *
 * Version History
 * ---------------
 * 1.0 2017-07-25 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _getCloudSQLTableProperties_(projectId, sqlInstance, sqlSchema, sqlTable, maxDate, nullMarker) {
  
  //Create JSON object to act as a container allowing multiple return values.
  var response = {
    tablePK : [], 
    pkFieldExists : null,
    tableDescription : 'unknown', 
    columnName : [],
    columnDataType : [],
    columnDescription : [],
    updateFieldExists : null,
    exportAllSQL : 'unknown',
    exportDeltaSQL : 'unknown',
    rowCountAll : -1, 
    rowCountDelta : -1, 
    nullMarker : null, 
    toString : 'unknown'
  }; 
  
  //Replace optional nullMarker parameter with default value.
  if(nullMarker == undefined) {
    nullMarker = '';
  }
  
  var dbStmt = _getCloudSQLStatement_(projectId, sqlInstance, sqlSchema);

  try {
    //Add table description to JSON object -----------------------------------------------------------------------------
    var dbSQL = "";
    dbSQL = dbSQL + " SELECT TABLE_COMMENT FROM information_schema.tables ";
    dbSQL = dbSQL + " WHERE ";
    dbSQL = dbSQL + " table_schema='" + sqlSchema + "'";
    dbSQL = dbSQL + " AND";
    dbSQL = dbSQL + " table_name='" + sqlTable + "'";

    // Execute the SQL Statement and get the first element of the query result set
    var rs = dbStmt.executeQuery(dbSQL);
    rs.next();
    response.tableDescription = rs.getString(1);     
    //------------------------------------------------------------------------------------------------------------------
     
    //Add all column name(s) to  JSON object ---------------------------------------------------------------------------
    var dbSQL = "";
    dbSQL = dbSQL + " SELECT COLUMN_NAME FROM information_schema.columns ";
    dbSQL = dbSQL + " WHERE ";
    dbSQL = dbSQL + " table_schema='" + sqlSchema + "'";
    dbSQL = dbSQL + " AND";
    dbSQL = dbSQL + " table_name='" + sqlTable + "'";
 
    // Execute the SQL Statement and get the column names of the source table
    var rs = dbStmt.executeQuery(dbSQL);
    while(rs.next()) {
      var columnName = rs.getString(1); 
      response['columnName'].push(columnName);
    }          
    //------------------------------------------------------------------------------------------------------------------
          
    //Flag existence or absence of G_DELTA_COLUMN_UPDATED column ------------------------------------------------------------------
    if(response['columnName'].indexOf(G_DELTA_COLUMN_UPDATED) >= 0) {
      response.updateFieldExists = true;
    } else {
      response.updateFieldExists = false;  
    }
    //------------------------------------------------------------------------------------------------------------------
          
    //Add primary key column(s) names to  JSON object ------------------------------------------------------------------
    var dbSQL = "";
    dbSQL = dbSQL + " SELECT k.COLUMN_NAME FROM information_schema.table_constraints t";
    dbSQL = dbSQL + " LEFT JOIN information_schema.key_column_usage k";
    dbSQL = dbSQL + " USING(constraint_name,table_schema,table_name)";
    dbSQL = dbSQL + " WHERE t.constraint_type='PRIMARY KEY'";
    dbSQL = dbSQL + " AND t.table_schema='" + sqlSchema + "'";
    dbSQL = dbSQL + " AND t.table_name='" + sqlTable + "'";
 
    // Execute the SQL Statement and get the primary key(s) of the source table
    var rs = dbStmt.executeQuery(dbSQL);
    while(rs.next()) {
      var tablePK = rs.getString(1); 
      response['tablePK'].push(tablePK);
    }
    //------------------------------------------------------------------------------------------------------------------
           
    //Flag existence or absence of primary key column(s) ---------------------------------------------------------------
    if(response['tablePK'].length > 0) {
      response.pkFieldExists = true;
    } else {
      response.pkFieldExists = false;  
    }
    //------------------------------------------------------------------------------------------------------------------
          
    //Create table structure definition --------------------------------------------------------------------------------  
    //Original inLineSchema statement, replaced with 3 seperate arrays.
    //dbSQL = dbSQL + " SELECT GROUP_CONCAT(CONCAT(COLUMN_NAME, ':', DATA_TYPE, ':', COLUMN_COMMENT))";
          
    // Create a comma separated list of fields with : delimited data type (e.g. field1:int:comment1,field2:varchr:comment2,...)
    var dbSQL = "";
    dbSQL = dbSQL + " SELECT DATA_TYPE";
    dbSQL = dbSQL + " FROM INFORMATION_SCHEMA.COLUMNS ";
    dbSQL = dbSQL + " WHERE TABLE_NAME = '" + sqlTable + "'";
    dbSQL = dbSQL + " AND TABLE_SCHEMA = '" + sqlSchema + "'";
    dbSQL = dbSQL + " ORDER BY ORDINAL_POSITION";
 
    // Execute the SQL Statement and get the primary key(s) of the source table
    var rs = dbStmt.executeQuery(dbSQL);
    while(rs.next()) {
      var columnDataType = rs.getString(1); 
      response['columnDataType'].push(columnDataType);
    }

    // Create a comma separated list of fields with : delimited data type (e.g. field1:int:comment1,field2:varchr:comment2,...)
    var dbSQL = "";
    dbSQL = dbSQL + " SELECT COLUMN_COMMENT";
    dbSQL = dbSQL + " FROM INFORMATION_SCHEMA.COLUMNS ";
    dbSQL = dbSQL + " WHERE TABLE_NAME = '" + sqlTable + "'";
    dbSQL = dbSQL + " AND TABLE_SCHEMA = '" + sqlSchema + "'";
    dbSQL = dbSQL + " ORDER BY ORDINAL_POSITION";

    // Execute the SQL Statement and get the primary key(s) of the source table
    var rs = dbStmt.executeQuery(dbSQL);
    while(rs.next()) {
      var columnDescription = rs.getString(1); 
      response['columnDescription'].push(columnDescription);
    }         
    //------------------------------------------------------------------------------------------------------------------

    // Write SQL statement to select All row in source -----------------------------------------------------------------
          
    //Without this line the results will be cropped and the SQL created might fail.
    var dbSQL = "";
    dbSQL = " SET SESSION group_concat_max_len = 1000000;";
    var rs = dbStmt.executeQuery(dbSQL);
          
    // Create the SQL Statement to select all columns and convert NULLS to blanks cells
    //The \n (new line) special character must be replace with \r (carriage return) 
    //because the \n is converted to a double quote (") when output to the CSV file
    //causing data loading failures.
    //This can be safely applied to all column types, even numbers and dates thankfully.
    //dbSQL = dbSQL + " SELECT CONCAT('SELECT ', GROUP_CONCAT(CONCAT(\"IFNULL(\",COLUMN_NAME,\",'') AS \", COLUMN_NAME)),' FROM ',TABLE_NAME)";
    var dbSQL = "";
    dbSQL = dbSQL + " SELECT CONCAT('SELECT ', GROUP_CONCAT(";
    dbSQL = dbSQL + "CONCAT(\"replace(IFNULL(`\",COLUMN_NAME,\"`,'" + nullMarker + "'), '\\n', '\\r') AS `\", COLUMN_NAME, \"`\")), ";
    dbSQL = dbSQL + "' FROM ',TABLE_NAME)";  
    dbSQL = dbSQL + " FROM INFORMATION_SCHEMA.COLUMNS ";
    dbSQL = dbSQL + " WHERE TABLE_NAME = '" + sqlTable + "'";
    dbSQL = dbSQL + " AND TABLE_SCHEMA = '" + sqlSchema + "'";
    dbSQL = dbSQL + " ORDER BY ORDINAL_POSITION";
          
    // Execute the SQL Statement and get the first element of the query result set
    var rs = dbStmt.executeQuery(dbSQL);
    rs.next();
    response.exportAllSQL = rs.getString(1);       
    //------------------------------------------------------------------------------------------------------------------

    // Write SQL statement to select only Delta rows in source ---------------------------------------------------------
    response.exportDeltaSQL = response.exportAllSQL;
    if(response.updateFieldExists && maxDate != '' && maxDate != 'null' && maxDate != null) {
      response.exportDeltaSQL += " where " + G_DELTA_COLUMN_UPDATED + " > '" + maxDate + "'";
    }
    //------------------------------------------------------------------------------------------------------------------
          
    //Count the All row(s) in source -----------------------------------------------------------------------------------
    var dbSQL = "SELECT COUNT(*) FROM (" + response.exportAllSQL + ") exportTable";	          
    var rs = dbStmt.executeQuery(dbSQL);
    rs.next();
    response.rowCountAll = parseInt(rs.getString(1)); 
    //------------------------------------------------------------------------------------------------------------------

    //Count the Delta row(s) in source ---------------------------------------------------------------------------------
    var dbSQL = "SELECT COUNT(*) FROM (" + response.exportDeltaSQL + ") exportTable ";          
    var rs = dbStmt.executeQuery(dbSQL);
    rs.next();
    response.rowCountDelta = parseInt(rs.getString(1)); 
    //------------------------------------------------------------------------------------------------------------------

    //Summarise all information into a string for easier debug ---------------------------------------------------------
    var toString = ''; 
    toString = toString + '\n';
    toString = toString + '--------------------------------------------- \n';
    toString = toString + '[' + projectId + '].[' + sqlInstance + '].[' + sqlSchema + '].[' + sqlTable + '] \n';
    toString = toString + 'tablePK length = ' + response.tablePK.length + ' \n';
    toString = toString + 'pkFieldExists = ' + response.pkFieldExists + ' \n';
    toString = toString + 'tableDescription = ' + response.tableDescription + ' \n';
    toString = toString + 'columnName length = ' + response.columnName.length + ' \n';
    toString = toString + 'columnDataType length = ' + response.columnDataType.length + ' \n';
    toString = toString + 'columnDescription length = ' + response.columnDescription.length + ' \n';
    toString = toString + 'updateFieldExists = ' + response.updateFieldExists + ' \n';
    toString = toString + 'exportAllSQL = ' + response.exportAllSQL + ' \n';
    toString = toString + 'exportDeltaSQL = ' + response.exportDeltaSQL + ' \n';
    toString = toString + 'rowCountAll = ' + response.rowCountAll + ' \n'; 
    toString = toString + 'rowCountDelta = ' + response.rowCountDelta + ' \n'; 
    toString = toString + 'nullMarker = ' + nullMarker + ' \n'; 
    toString = toString + '--------------------------------------------- \n';
    response.toString = toString;
    //------------------------------------------------------------------------------------------------------------------

    //Add Null Marker --------------------------------------------------------------------------------------------------
    response.nullMarker = nullMarker;
    //------------------------------------------------------------------------------------------------------------------
    
    // Close the connections
    rs.close();
    dbStmt.close(); 
          
    return response;

  } catch(err) {
    _log_('ERROR', 'Getting Cloud SQL table properties failed: ' + err);
    return null;
  }   
}
