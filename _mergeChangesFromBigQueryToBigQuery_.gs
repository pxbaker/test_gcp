/***********************************************************************************************************************
 * Function to merge the contents of two big query datasets, an original table of data and a delta table containing new 
 * or changed rows of data.  
 * If the row is new (the PK only exists in the delta table) it will be included from the delta table in the output 
 * table.
 * If the row is changed (the PK exists in both the original table and delta table) it will be included from the delta 
 * table in the output table.
 * If the row is unchanged (the PK only exists in the original table) it will be included from the original table in 
 * the output table.
 * This logic does not support the removal/deletion of any rows from the original table, i.e. row are only ever 
 * inserted or updated.
 *
 * @projectId     {string} input the Project ID where both the original and delta tables reside.
 * @bqDataset     {string} input the BigQuery dataset ID where both the original and delta tables reside.
 * @sqlTable      {string} input the BigQuery table ID containing the original data and the output data.
 * @bqDeltaTable  {string} input the BigQuery table ID containing the delta data (new and changed rows).
 * @tablePKs      {string} input an array of primary key column names in the table.
 * @columnNames   {string} input an array of all column names in the table.
 *
 * @return {boolean} returns true if successful and false if an error was encountered.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-01 Created
 * 1.1 2017-09-08 Replaced materialiseViewDefnToBigQuery with copyDefnToBigQueryTable call.
 **********************************************************************************************************************/
function _mergeChangesFromBigQueryToBigQuery_(projectId, bqDataset, sqlTable, bqDeltaTable, tablePKs, columnNames) {
  //Merge Delta  ---------------------------------------------------------------------------------------
  //When the columnName and tablePK arrays are converted to strings they become comma seperated values
  //which fit's the SQL syntax.
  var bqMergeDefn = '';
  bqMergeDefn = bqMergeDefn + ' SELECT ' + columnNames;
  bqMergeDefn = bqMergeDefn + ' FROM ';
  bqMergeDefn = bqMergeDefn + '   ( ';
  bqMergeDefn = bqMergeDefn + '   SELECT COUNT(*) OVER (PARTITION BY ' + tablePKs + ') AS row_count, * '; 
  bqMergeDefn = bqMergeDefn + '   FROM ';
  bqMergeDefn = bqMergeDefn + '     (SELECT 0 AS delta_row, * ';
  bqMergeDefn = bqMergeDefn + '     FROM ['+projectId+':'+bqDataset+'.'+sqlTable+']), '; 
  bqMergeDefn = bqMergeDefn + '     (SELECT 1 AS delta_row, * ';
  bqMergeDefn = bqMergeDefn + '     FROM ['+projectId+':'+bqDataset+'.'+bqDeltaTable+']) '; 
  bqMergeDefn = bqMergeDefn + '   ) ';
  bqMergeDefn = bqMergeDefn + ' WHERE row_count = 1 OR (row_count = 2 AND delta_row = 1) ';   
  //Merge Delta  ---------------------------------------------------------------------------------------
      
  // Merge the delta BigQuery table with the target table to apply the updates
  return copyDefnToBigQueryTable(projectId, bqMergeDefn, bqDataset, sqlTable, false);
}
