/***********************************************************************************************************************
 * Function converts a string into an boolean, it also supports a boolean as the input and simply returns it directly 
 * back unchanged.  The use case comes from a limitation in the BODS to DLF interface, specifically BODS inability to 
 * provide parameters as anything other than strings.  To avoid forcing this limitation onto all DLF function signatures 
 * the work around will be implemented to check any booleans provided at the beginning of the function then covert them 
 * if necessary.
 * 
 * @stringOrBooleanValue {object} input is a boolean or string.  Valid values; "True" (case insensitive), "False" 
 *                                (case insensitive), "1", "0", 1 or 0.
 *
 * @return {boolean} output is a boolean value if successfully converted and null if it failed.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-15 Created.
 * 1.1 2017-10-17 Support for numeric representations of true and false added.  Strings trimmed to allow leading and 
 *                trailing spaces.
 **********************************************************************************************************************/
function _convertStringToBoolean_(stringOrBooleanValue) {
  
  //Default to NULL if conversion not possible to convert.
  var booleanValue = null;
  
  if(typeof(stringOrBooleanValue) == 'boolean') {
    booleanValue = stringOrBooleanValue;
  } else {
    //Prepare string to simplify comparison conditions.
    stringOrBooleanValue = String(stringOrBooleanValue).toUpperCase();
    stringOrBooleanValue = stringOrBooleanValue.trim(); 
    
    if(stringOrBooleanValue == 'TRUE' || stringOrBooleanValue == '1') {
      booleanValue = true;
    } else if(stringOrBooleanValue == 'FALSE' || stringOrBooleanValue == '0') {
      booleanValue = false;
    } else { 
      _log_('ERROR', 'Unable to convert string "' + stringOrBooleanValue + '" to a boolean value');	
    }
  }
  
  return booleanValue;
}