/***********************************************************************************************************************
 * Environment Variables 
 * These values are used by functions across the full scope of the Google Apps Script project.
 *
 * Warning: 
 * This script contains environment specific code.  Do NOT promote the complete script, migrated changed lines only.
 *
 * Version History
 * ---------------
 * 1.0 Created
 * 1.1 Added G_VERSION variable.
 * 1.2 2017-10-02 Added G_CS_BUCKET_EMPTY_FILE and G_CS_EMPTY_FILE_NAME variables.
 **********************************************************************************************************************/  

//This only tracks the main releases
var G_VERSION = 'v2.0.0 (BODS QA) jlr-dl-edw-test'; 

var G_CS_BUCKET_EMPTY_FILE = 'jlr-dl-edw-test-framework-test';
var G_CS_EMPTY_FILE_NAME = 'empty_file.csv';

//dlf-billing-sa (jlr-dl-edw-test)
//https://script.google.com/macros/d/1uGx_22NIxOr5TFt_ayqDqIhdtE7eBJea92RW7891YcNVTur7OhGSV3W/usercallback
var CLIENT_ID = '163363378028-sum8ue352ee27g5ra9o8ds3ro87k8sbm.apps.googleusercontent.com';
var CLIENT_SECRET = 'HH1XlMCPvSYAaBigZcxC6lgK';

//Cloud SQL Enterprise account.
//jlr-dl-edw-test account
var G_SERVICE_ACCOUNT = 'dlf-sa';
var G_SERVICE_PASSWORD = 'vnHw4893tyh34g!';

var G_SERVER_SSL_CERTIFICATE = '-----BEGINCERTIFICATE-----\n'+
 'MIIDITCCAgmgAwIBAgIBADANBgkqhkiG9w0BAQUFADBIMSMwIQYDVQQDExpHb29n'+
 'bGUgQ2xvdWQgU1FMIFNlcnZlciBDQTEUMBIGA1UEChMLR29vZ2xlLCBJbmMxCzAJ'+
 'BgNVBAYTAlVTMB4XDTE2MDQyODEyMTU1MVoXDTE4MDQyODEyMTY1MVowSDEjMCEG'+
 'A1UEAxMaR29vZ2xlIENsb3VkIFNRTCBTZXJ2ZXIgQ0ExFDASBgNVBAoTC0dvb2ds'+
 'ZSwgSW5jMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC'+
 'ggEBANLLTg7Mujv9SIs06JExKCwix2loGaumgcTG38rXejCl9Z0JycbSx1VjHnR6'+
 'mJuvRxX4rFV12Ly0brDcdtvlrqMWygWk/lwCEt9rn/TbIi6lx8wUxHXxnudLG78m'+
 'YJGLeZIYsclqhdxqryZjeskNF7LwCF8jQ8hBHLHao6UGWVgikTnjBEpyqwLx4c0t'+
 'a3RI9jDegwRZN9+RQNKXC5xVsD4kX2r4sj3VeaA0bPcQHM7PFamZpB3zJ6T1tZeA'+
 'IQcCxHchvG5ftXX2pc6XGsZBfVExSa6mboIe84/CE4VBh6s9ItHXYm0mNxbHy2nl'+
 '5iM2SQFR2gVHtkOjyjn1aCDOEJkCAwEAAaMWMBQwEgYDVR0TAQH/BAgwBgEB/wIB'+
 'ADANBgkqhkiG9w0BAQUFAAOCAQEARhUp7pvI4Go7w9BhrvIK7NEvz1LaR96t0SHx'+
 'P5hO/davoE2DCSwiCWYjwSp2dW7Cs0CO1bPONyH/9pfmkW67dDinVVz+f2ktEOHi'+
 'kaBTOK6hzguC9IObM0OoEKjrAXZOutkTH97C59a+v5nEyd2hTW3ez00W7d4inryv'+
 'AavPu4AdzsahgAfXH4XSgEcnCyla0PXJGd5YpQTrZN1wnsGD19dpI2RlxpT4m8C7'+
 '4FtpAWqZGffhSSDDsfh4PcsSL0tnN0GAC4h0owm+6oaEDJVYMutR36A2UHs2Pk3y'+
 'BFWgtJpxKxU1e3Gu1B+Ar1Mtz6o8riw5dxRJ5GE9VcjVot03oQ=='+
 '\n-----ENDCERTIFICATE-----';

var G_CLIENT_SSL_CERTIFICATE = '-----BEGINCERTIFICATE-----\n'+
 'MIIDUjCCAjqgAwIBAgIEK4wvQzANBgkqhkiG9w0BAQUFADBjMT4wPAYDVQQDEzVH'+
 'b29nbGUgQ2xvdWQgU1FMIENsaWVudCBDQSBjb2UtcG9jIGNsaWVudCBjZXJ0aWZp'+
 'Y2F0ZTEUMBIGA1UEChMLR29vZ2xlLCBJbmMxCzAJBgNVBAYTAlVTMB4XDTE2MDcw'+
 'NzEyMTU0MloXDTE4MDcwNzEyMTY0MlowSDEjMCEGA1UEAxMaY29lLXBvYyBjbGll'+
 'bnQgY2VydGlmaWNhdGUxFDASBgNVBAoTC0dvb2dsZSwgSW5jMQswCQYDVQQGEwJV'+
 'UzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKIewTaCawTJzuKu5c8M'+
 'k7Lr8P3ex7T04PUlz9qxswip4q6nwGOjz/du8ne4rqFusHBP6QjrZ0OXRgxf7PSC'+
 'mk5hW8GY4I4F25mw2EWErPLlLlK6aXcH7N270PsoKU6/Mf3WJdmg+p+KOnGnTLnr'+
 'OKPCSNkHl++eqTQoGsxElFsQ439WOM/aRJvzpQCd90mfMZ18831Y9CpweU73vZAb'+
 'N+gzvdVS5/71BUqzmSfgRQQbqUuOgppjjzi4jEbDPYsU+sj6x4LRkKNcqdwaTFwM'+
 'ccCiXmZaxK1RQsiOhUGnPxv6Ovgr27I2hjM1ihsN+WTYZ7fZLOWjSuw/2NIRbhff'+
 '9A8CAwEAAaMpMCcwJQYDVR0RBB4wHIEabWZyaXNlQGphZ3VhcmxhbmRyb3Zlci5j'+
 'b20wDQYJKoZIhvcNAQEFBQADggEBACTmDdUaHQHhN/KOMsk9bZcKvehBIXmRd4tT'+
 'PX1MhsYSrlf/h/fPl3dy/DlfZlt1arKghZLoD/tJ03W5naVD6X52cvbSB0R2dwHd'+
 'ATMI/uDKONtm5kJJmZeJf44t1X2QW4YOwgMrfsK1iam8NOfNj7XwEJtVug8UkyAO'+
 'ySMAuYHZQnkX5wp+wJZC6E8OQnAu6atcj7wlCJrlshvB+sul1BlLfR1adLyHJdIM'+
 'FzRa1BBE+VfBq78xOQG4qALFLfp0TV6cXkEwa0BR7hMQTscyQSN6vjLmpsi2d67b'+
 'p32dzqVshcxB9Yorw11takQUWhMqUwr3kzY3ttz2ZnCoYmfA5JE='+
 '\n-----ENDCERTIFICATE-----';

var G_SERVER_SSL_KEY = '-----BEGINRSAPRIVATEKEY-----\n'+
 'MIIEogIBAAKCAQEAoh7BNoJrBMnO4q7lzwyTsuvw/d7HtPTg9SXP2rGzCKnirqfA'+
 'Y6PP927yd7iuoW6wcE/pCOtnQ5dGDF/s9IKaTmFbwZjgjgXbmbDYRYSs8uUuUrpp'+
 'dwfs3bvQ+ygpTr8x/dYl2aD6n4o6cadMues4o8JI2QeX756pNCgazESUWxDjf1Y4'+
 'z9pEm/OlAJ33SZ8xnXzzfVj0KnB5Tve9kBs36DO91VLn/vUFSrOZJ+BFBBupS46C'+
 'mmOPOLiMRsM9ixT6yPrHgtGQo1yp3BpMXAxxwKJeZlrErVFCyI6FQac/G/o6+Cvb'+
 'sjaGMzWKGw35ZNhnt9ks5aNK7D/Y0hFuF9/0DwIDAQABAoIBAGf5sZF0M47un3Fj'+
 'GqCgQFoQWI95wjuckmqnxZdNQLXdvzSlAg7lkQvwbDG8kMOYajCeD6dY0vHWo2Zs'+
 'X1yyJqT+bArOMbbkquL3OL3dSc6+jechB2MAAjpzqqRqLZQDq9xQ7BWX2SVZUgJy'+
 'ehMDeWANywrmg9t6tqpszkMT+NIuDVIC86t+ktDt4L7sEmqOFp+irFLfA3wnaW92'+
 '2t4+9jfwhLYxXeZs1DG2fUy9oJps411J0aisgsZExGykNNIOy+iZZvo6oIW1Mn6Y'+
 'VMnFiIvYM0xowtLWuBbTBdfP+7zSaFzDW0nf7FyGhaq9xpydiNdJlpZ014IHlEpJ'+
 '3KCMVwECgYEA0R6rXLQyu1uR/VQnaYiZPy6Nz9er2znuf0qN30E4kExMIybz+ctf'+
 'sNDDGIboWamGStwrME87/YND/0K8kBmoQwc8X/ejDjeIYMYGE/h6BIdouf8vapqm'+
 'NxtPBBrGXIlKQn8om6mGcvAC14eG0+132r9oz0Q8JWDNVLoPoN5ngkcCgYEAxnbJ'+
 'bKKbwUm7fkbwpl5Nwpl2hXPoZ848jvnKLt4sfoprkkJ1Bs8MJ122ItzVV0dmtDnn'+
 'wwcMaJFzUPpsZ+xiTZbVZ7dp/FQdOaqGs/EzZjLi6tS6Bc5MQhGNremlB4adbHFX'+
 'GuwqC0sS6mg9RXbEZzplwbX+G8IGTvjuj92FW/kCgYBw8AFYJbGoYgHsMMDATBeH'+
 'edr4vt9/nD/j2x3Xzt9/ftU3vtfQtjdNxNE4s9NUaSuvzkw84O3jMjtM5nvzzweC'+
 'e6vtoE4dZXxv10J9EprDS2YR3XF8P105MbRw4Qzz+Ol0FDyYJOwJsvm18OmuFve9'+
 '2dX+6jkZbrg14+6A0NxSDQKBgCN6sN+e4kFXsREOFY9oZHZZgPqykeWc1O6qcR6X'+
 '0VR94mUvRESHVyDie+EXoXwM46tsfqY66Fj5R6g+XIrnk1tTj6sO+v+E+gSJG7On'+
 'dwzaI/7fMLJBAxj8klt43WvPAO2WBYBmn82NCkClie6VIN9jbTWIF3lW4lHT5rIJ'+
 'ZAHhAoGAG7PMM27QzgFA7Hjy7LinM6HtLMqv2PXjRD3SqrMax5b4qou2jnFADZAL'+
 '1hF4LLVhvy0eEi3eJL3LYLonp1mIWz6uoV6F9DrWnqsJL4+mmwFG9Gd7r8PYaz/V'+
 'tldouU2uNKX4UU5X5wEzxfnQLgzS6L+o/YSWti6lIJWQ0qIy4WU='+
 '\n-----ENDRSAPRIVATEKEY-----';