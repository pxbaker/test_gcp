//Remove after version 2.0.0 has been tested
/***********************************************************************************************************************
 * Delegation of authority to a service account by jagularlandrover.com user.
 *
 * Warning: 
 * This script contains environment specific code.  Do NOT promote the complete script, migrated changed lines only.
 *
 * Version History
 * ---------------
 * 1.0 
 *  |  Released prior to the version history changes being documented.
 * 1.9 
 * 2.0 Header added.  
 **********************************************************************************************************************/ 
/*
//dlf-billing-sa (jlr-dl-edw-test)
//https://script.google.com/macros/d/1uGx_22NIxOr5TFt_ayqDqIhdtE7eBJea92RW7891YcNVTur7OhGSV3W/usercallback
var CLIENT_ID = '163363378028-sum8ue352ee27g5ra9o8ds3ro87k8sbm.apps.googleusercontent.com';
var CLIENT_SECRET = 'HH1XlMCPvSYAaBigZcxC6lgK';

// Global OAuth2.0 Access token.
// This is available to all subsequent scripts.
var token;

function authenticate() 
{
  	// Check we have access to the service
	var service = getService();
	if (!service.hasAccess()) {
		var authorizationUrl = service.getAuthorizationUrl();
		Logger.log('Open the following URL and re-run the script: %s', authorizationUrl);
		return;
	}

    Logger.log('Passed Authentication');
  
	// Get the Access Token
    token = service.getAccessToken();
}
*/