/***********************************************************************************************************************
 * Function to find out if a BigQuery table exists.
 *
 * @projectId  {string} input the Project ID where the table is expected to reside.
 * @bqDataset  {string} input the BigQuery dataset ID where the table is expected to reside.
 * @sqlTable   {string} input the BigQuery table name to be found.
 *
 * @return {boolean} output the function returns true if the table was found, otherwise false.
 *
 * Version History
 * ---------------
 * 1.0 2017-09-04 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _bigQueryTableExists_(projectId, bqDataset, sqlTable) {  
  try { 
    var job = BigQuery.Tables.get(projectId, bqDataset, sqlTable);
    //If this line is reached the get command didn't throw an exception, thus the table must exist.
    return true;
  } catch(err) { 
    //This might not be considered an error.
    _log_('DEBUG', 'Could not find BigQuery table "' + sqlTable + '"');	
    return false;   
  } 
}