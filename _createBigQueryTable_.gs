/***********************************************************************************************************************
 * Function to get the maximum value from a column in a BigQuery table.  Useful when performing delta operations to get
 * the highest last created or updated date and time value.
 *
 * @projectId           {string} input the Project ID where the table is to be created.
 * @datasetId           {string} input the BigQuery dataset ID where the table is to be created.
 * @tableId             {string} input the BigQuery table name to create.
 * @columnNamess        {string} input an array of names for each column in the table.
 * @bqColumnDataTypess  {string} input an array of data types for each column in the table.
 * @columnDescriptionss {string} input an array of descriptions for each column in the table.
 * @tableDescription    {string} input the description for the table.
 *
 * @return {boolean} output the function returns true if the table was created successfully and false if an error was 
 *                   encountered. 
 *
 * Version History
 * ---------------
 * 1.0 2017-09-05 Created.
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 **********************************************************************************************************************/
function _createBigQueryTable_(projectId, datasetId, tableId, columnNames, bqColumnDataTypes, columnDescriptions, 
                               tableDescription) {

  // Create the table definition
  var tableDef = {
    tableReference: {
      projectId: projectId,
      datasetId: datasetId,
      tableId: tableId
    },
    description : tableDescription, 
    schema: {
      fields: []
    }
  };	

  //Loop through each column...
  for (var i=0; i < columnNames.length; i++) {
    tableDef['schema'].fields.push(
      {
        name: columnNames[i], 
        type: bqColumnDataTypes[i], 
        description: columnDescriptions[i]
      }
    );
  }
  
  try{    
    tableResult = BigQuery.Tables.insert(tableDef, projectId, datasetId);
    
    //Creation is asynchronous, this loop keeps the function synchronous.
    while (!_bigQueryTableExists_(projectId, datasetId, tableId)) {
      Utilities.sleep(100); 
    }
  } catch(err) {
    _log_('DEBUG', 'Table Definition: ' + tableDef);
    _log_('ERROR', 'Create BigQuery table failed: '  + err);
    return false;
  }
  
  _log_('DEBUG', 'Created BigQueryTable [' + projectId + '].[' + datasetId + '].[' + tableId + ']');
  
  return true;
}
