/***********************************************************************************************************************
 * This function confirms the existence or absence of a specified file in Cloud Storage.
 *
 * @csBucket   {string} input the bucket name.  Google Cloud Storage uses a flat namespace, so all bucket names are 
 *                      unique even without the inclusion of the project name. 
 * @csFileName {string} input the file name, prefixed with the folder path if one exists.  
 *                      E.g. "your_folder/your_file.txt"
 * 
 * @return {boolean} returns true if file exists and false if not found.
 *
 * Version History
 * ---------------
 * 1.0 2017-07-13 Created
 * 1.1 2017-09-08 Replaced all Logger.log() with _log_() calls.
 * 1.2 2017-09-20 Improved debug message to differentiate between a bucket, folder and a file check.
 **********************************************************************************************************************/ 
function _cloudStorageFileExists_(csBucket, csFileName) {
  
  var token = _getAuthenticationToken_();
  
  //Differentiate between a bucket, folder or file existence check.
  if(csFileName == '') {
    _log_('DEBUG', 'Checking for the existence of bucket: gs://' + csBucket + ' ');
  } else if (csFileName.substr(csFileName.length - 1) == '/') {
    _log_('DEBUG', 'Checking for the existence of folder: gs://' + csBucket + '/' + csFileName);
  } else {
    _log_('DEBUG', 'Checking for the existence of file: gs://' + csBucket + '/' + csFileName);
  }
  
  try {
    // Create the header authorisation  
    var headers = {
      "Authorization": "Bearer " + token
    };
    // Create the Cloud SQL instances get parameters
    var parameters = {
      "method": "get",    
      "headers": headers,
      "muteHttpExceptions": true
    };
    
    // Create the url of the storage API
    var api = "https://www.googleapis.com/storage/v1/b/" + csBucket + "/o/" + encodeURIComponent(csFileName);
    // Use the url fetch service to issue the https request and capture the response
    
    var response = UrlFetchApp.fetch(api, parameters); 
    
    if (response.getResponseCode() == 200) {
      return true;
    } else if (response.getResponseCode() == 404) {
      return false;
    }
    _log_('ERROR', 'unexpected response (' + response.getResponseCode() + ') ' + response.getContentText());
    return false;
    
  } catch(err) {
    _log_('ERROR', 'Checking file gs://' + csBucket + '/' + csFileName + ': '  + err);
    return false;
  }    	
}